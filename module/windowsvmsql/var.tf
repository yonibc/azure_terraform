variable "prefix" {
  default = "eptest"
}
variable "location" {
  default = "eastus2"
}
variable "subnetid" {
  default = ""
}
 variable "size" {
   default = "Standard_DS2_v2"
 }