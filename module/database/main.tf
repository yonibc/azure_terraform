resource "azurerm_mysql_server" "mysql" {
  name = "${var.prefix}-mysql-server1"
  location = var.location
  resource_group_name = azurerm_resource_group.main.name
  storage_mb = "102400"
  sku_name = "MO_Gen5_2"


  administrator_login = "epromoadmin"
  administrator_login_password = "Br@nd3dPr0duct$"
  version = "5.7"
  ssl_enforcement_enabled = "false"
  tags = {
    Name =var.prefix
  }
}

resource "azurerm_mysql_virtual_network_rule" "mysqlvnet" {
  name                = "mysql-vnet-rule"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_mysql_server.mysql.name
  subnet_id           = var.subnetid
}

resource "azurerm_mysql_firewall_rule" "stcloud" {
  name                = "office"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_mysql_server.mysql.name
  start_ip_address = "209.237.103.161"
  end_ip_address = "209.237.103.169"
  }

resource "azurerm_mysql_database" "eoe" {
  name                = "epromos2_eoe"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_mysql_server.mysql.name
  charset             = "latin1"
  collation           = "latin1_swedish_ci"
}

resource "azurerm_mysql_database" "sessions" {
  name                = "livesessions"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_mysql_server.mysql.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_database" "alfresco" {
  name                = "alfresco_community_5_1_epromos_eoe"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_mysql_server.mysql.name
  charset             = "latin1"
  collation           = "latin1_swedish_ci"
}

resource "azurerm_mysql_database" "epromos2" {
  name                = "epromos2_wordpress"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_mysql_server.mysql.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_database" "blog" {
  name                = "blogepromos"
  resource_group_name = azurerm_resource_group.main.name
  server_name         = azurerm_mysql_server.mysql.name
  charset             = "latin1"
  collation           = "latin1_swedish_ci"
}