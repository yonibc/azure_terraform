variable "prefix" {
  default = "eptest"
}
variable "location" {
  default = "eastus2"
}
variable "resource_group" {
  default = "main"
}
variable "mysql_connection" {
  default = "root@localhost:3306/mysql"
}
