resource "azurerm_app_service" "appservice" {
  name                = "${var.prefix}-app-plan"
  location            = var.location
  resource_group_name = azurerm_resource_group.main.name
  app_service_plan_id = azurerm_app_service_plan.appservice_plan.id

  site_config {
    php_version = "7.3"
    scm_type    = "BitbucketGit"
  }
  tags = {
    Name = var.prefix
  }
}