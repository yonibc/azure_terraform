resource "azurerm_resource_group" "main" {
  name     = var.prefix
  location = var.location
}
resource "azurerm_app_service_plan" "appservice_plan" {
  name                = "${var.prefix}-app-plan"
  location            = var.location
  resource_group_name = azurerm_resource_group.main.name

  sku {
    tier = "Standard"
    size = "S1"
  }
  tags = {
    Name = var.prefix
  }
}