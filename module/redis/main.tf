resource "azurerm_redis_cache" "rediscache" {
  name                = "${var.prefix}-cache"
  location            = var.location
  resource_group_name = azurerm_resource_group.main.name
  capacity            = 2
  family              = "C"
  sku_name            = "Standard"
  enable_non_ssl_port = false
  minimum_tls_version = "1.2"

  redis_configuration {
  }

  tags = {
    Name = var.prefix
  }
}