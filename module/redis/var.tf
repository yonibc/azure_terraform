variable "prefix" {
  default = "eptest"
}
variable "location" {
  default = "eastus2"
}
variable "resource_group" {
  default = "main"
}