variable "prefix" {
  default = "eptest"
}
variable "location" {
  default = "eastus2"
}
variable "resource_group" {
  default = "main"
}
variable "nodes" {
  default = "2"
}
variable "subnetid" {
  default = ""
}
variable "imageid" {
  default = ""
}
variable "size" {
   default = "Standard_DS2_v2"
 }