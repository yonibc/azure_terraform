resource "azurerm_resource_group" "main" {
  name     = var.prefix
  location = var.location
}
##network access/security groups
resource "azurerm_network_security_group" "nsg" {
    name                = "${var.prefix}-NetworkSecurityGroup"
    location            = var.location
    resource_group_name = azurerm_resource_group.main.name
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "HTTP"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "HTTPS"
        priority                   = 1003
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}
resource "azurerm_public_ip" "publicip" {
  name = "${var.prefix}-publicip"
  location = var.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method = "Dynamic"
  domain_name_label = var.prefix
  tags = {
    Name = var.prefix
  }
}
resource "azurerm_network_interface" "nic" {
  name                = "${var.prefix}-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "nicconfig1"
    subnet_id                     = var.subnetid
    private_ip_address_allocation = "Dynamic"
  }
  tags = {
    Name = var.prefix
  }
}
resource "azurerm_network_interface_security_group_association" "nicnsg" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}
##environment diagnostic storage account
resource "azurerm_storage_account" "diagstorageacct" {
  name = "${var.prefix}diag"
  resource_group_name = azurerm_resource_group.main.name
  location = var.location
  account_replication_type = "LRS"
  account_tier = "Standard"

  tags = {
    environment = var.prefix
  }
}

##environment diagnostic storage account
resource "azurerm_storage_account" "vhdisks" {
  name = "${var.prefix}disks"
  resource_group_name = azurerm_resource_group.main.name
  location = var.location
  account_replication_type = "LRS"
  account_tier = "Standard"

  tags = {
    environment = var.prefix
  }
}
resource "azurerm_storage_container" "vhds" {
  name                  = "vhds"
  storage_account_name  = azurerm_storage_account.vhdisks.name
  container_access_type = "private"
}

resource "azurerm_lb" "eplb" {
  name                = "${var.prefix}eplb"
  location            = var.location
  resource_group_name = azurerm_resource_group.main.name

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.publicip.id
  }
}

resource "azurerm_lb_backend_address_pool" "bpepool" {
  resource_group_name = azurerm_resource_group.main.name
  loadbalancer_id     = azurerm_lb.eplb.id
  name                = "BackEndAddressPool"
}

resource "azurerm_lb_nat_pool" "lbnatpool" {
  resource_group_name            = azurerm_resource_group.main.name
  name                           = "backend-pool"
  loadbalancer_id                = azurerm_lb.eplb.id
  frontend_ip_configuration_name = "PublicIPAddress"
  protocol                       = "Tcp"
  frontend_port_start            = 80
  frontend_port_end              = 90
  backend_port                   = 80
}

resource "azurerm_lb_probe" "eplbprobe" {
  resource_group_name = azurerm_resource_group.main.name
  loadbalancer_id     = azurerm_lb.eplb.id
  name                = "ssh-running-probe"
  port                = 22
  protocol            = "Tcp"
}
resource "azurerm_lb_rule" "main" {
  name                           = "lb-rule"
  resource_group_name            = azurerm_resource_group.main.name
  loadbalancer_id                = azurerm_lb.eplb.id
  probe_id                       = azurerm_lb_probe.eplbprobe.id
  backend_address_pool_id        = azurerm_lb_backend_address_pool.bpepool.id
  frontend_ip_configuration_name = "PublicIPAddress"
  protocol                       = "Tcp"
  frontend_port                  = 22
  backend_port                   = 22
}