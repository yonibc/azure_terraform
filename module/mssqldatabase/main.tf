resource "azurerm_sql_server" "mssql" {
  name = "${var.prefix}-server1"
  location = var.location
  resource_group_name = azurerm_resource_group.main.name
  version = "12.0"
  administrator_login = "motadmin"
  administrator_login_password = "M0t1v4t0r$Pr0duct5"

  tags = {
    Name = var.prefix
  }
}

resource "azurerm_sql_virtual_network_rule" "sqlvnetrule" {
  name = "${var.prefix}sqlvnet"
  resource_group_name = azurerm_resource_group.main.name
  server_name = azurerm_sql_server.mssql.name
  subnet_id = var.subnetid
}

resource "azurerm_sql_firewall_rule" "sqlfirewallrule" {
  name = "StCloudOffice"
  resource_group_name = azurerm_resource_group.main.name
  server_name = azurerm_sql_server.mssql.name
  start_ip_address = "209.237.103.162"
  end_ip_address = "209.237.103.168"
}

resource "azurerm_mssql_database" "admc" {
  name = "AdminConsole"
  server_id = azurerm_sql_server.mssql.id
  collation = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  sku_name       = "S3"
  zone_redundant = "false"
  max_size_gb = "50"

  tags = {
    Name = var.prefix
  }
}

resource "azurerm_mssql_database" "excel" {
  name = "ExcellonetMaster"
  server_id = azurerm_sql_server.mssql.id
  collation = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  sku_name       = "S3"
  zone_redundant = "false"
  max_size_gb = "50"

  tags = {
    Name = var.prefix
  }
}

resource "azurerm_mssql_database" "mkt" {
  name = "Marketing"
  server_id = azurerm_sql_server.mssql.id
  collation = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  sku_name       = "S3"
  zone_redundant = "false"
  max_size_gb = "50"

  tags = {
    Name = var.prefix
  }
}

resource "azurerm_mssql_database" "motstore" {
  name = "MotivatorsStore"
  server_id = azurerm_sql_server.mssql.id
  collation = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  sku_name       = "S3"
  zone_redundant = "false"
  max_size_gb = "50"

  tags = {
    Name = var.prefix
  }
}

resource "azurerm_mssql_database" "phonesales" {
  name = "Phone_Sales"
  server_id = azurerm_sql_server.mssql.id
  collation = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  sku_name       = "S3"
  zone_redundant = "false"
  max_size_gb = "50"

  tags = {
    Name = var.prefix
  }
}