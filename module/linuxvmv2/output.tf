output "vmid" {
  value = azurerm_linux_virtual_machine.azvm.id
}
output "vmprefix" {
  value = var.prefix
}
output "resourcegroup" {
  value = azurerm_resource_group.main.name
}