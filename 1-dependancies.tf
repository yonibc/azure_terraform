##provider
provider azurerm {
  version = "~>2.0"
  features  {}
}

## Azure encrypted state
terraform {
  backend "azurerm" {
    resource_group_name = "terraform"
    storage_account_name = "qatfstateepromos"
    container_name = "tfstate"
    key = "yWkWFL0TP7dlptqvGGhDdi1ISJyYqcFklUhyoAmj8vOdlOM15kOqb8JKerGJBU23aGgAzmKwai2Yp0Ud59BPUQ=="
    access_key = "yWkWFL0TP7dlptqvGGhDdi1ISJyYqcFklUhyoAmj8vOdlOM15kOqb8JKerGJBU23aGgAzmKwai2Yp0Ud59BPUQ=="
  }
}

##resource groups
resource "azurerm_resource_group" "main" {
  name     = var.prefix
  location = var.location
}
#storageacct
resource "azurerm_storage_account" "primaryacct" {
  name                     = "${var.prefix}stor"
  resource_group_name      = azurerm_resource_group.main.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags = {
    Name = var.prefix
  }
}
resource "azurerm_storage_container" "vhdcontainer" {
  name                  = "${var.prefix}-vhds"
  storage_account_name  = azurerm_storage_account.primaryacct.name
  container_access_type = "private"
}
#vm images
resource "azurerm_image" "centos8" {
  name    ="centos8"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  hyper_v_generation = "V2"
  os_disk {
    os_type  = "Linux"
    os_state = "Generalized"
    caching = "ReadWrite"
    blob_uri = "https://eptestmages.blob.core.windows.net/vhdimages/serverbaseimage.vhd"
    size_gb  = 32
  }
}

resource "azurerm_image" "tomcat" {
  name    ="tomcat"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  hyper_v_generation = "V2"
  os_disk {
    os_type  = "Linux"
    os_state = "Generalized"
    caching = "ReadWrite"
    blob_uri = "https://eptestmages.blob.core.windows.net/vhdimages/centos8.vhd"
    size_gb  = 32
  }
}

resource "azurerm_image" "wordpress" {
  name    ="wordpress"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  hyper_v_generation = "V1"
  os_disk {
    os_type  = "Linux"
    os_state = "Generalized"
    caching = "ReadWrite"
    blob_uri = "https://eptestmages.blob.core.windows.net/vhdimages/wordpress.vhd"
    size_gb  = 32
  }
}

resource "azurerm_image" "screamingfrog" {
  name = "screamingfrog"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  hyper_v_generation = "V2"
  os_disk {
    os_type  = "Linux"
    os_state = "Generalized"
    caching = "ReadWrite"
    blob_uri = "https://eptestmages.blob.core.windows.net/vhdimages/sfrog.vhd"
    size_gb  = 32
  }
}

resource "azurerm_image" "epromosqaendeca" {
  name    ="epromosqaendeca"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  hyper_v_generation = "V1"
  os_disk {
    os_type = "Linux"
    os_state = "Generalized"
    caching = "ReadWrite"
    blob_uri = "https://eptestmages.blob.core.windows.net/vhdimages/endeca.vhd"
    size_gb = 128
  }
}

##networks
resource "azurerm_virtual_network" "net" {
  name = "${var.prefix}-network"
  address_space = ["192.168.0.0/16"]
  location = var.location
  dns_servers = ["192.168.0.4","192.168.0.5"]
  resource_group_name = azurerm_resource_group.main.name
  tags = {
    Name = var.prefix
  }
}
resource "azurerm_subnet" "adsubnet" {
  name                 = "${var.prefix}-adsubnet"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.net.name
  address_prefixes       = ["192.168.0.0/24"]
  service_endpoints = ["Microsoft.Storage","Microsoft.AzureActiveDirectory"]
}
resource "azurerm_subnet" "subnet" {
    name                 = "${var.prefix}-subnet"
    resource_group_name  = azurerm_resource_group.main.name
    virtual_network_name = azurerm_virtual_network.net.name
    address_prefixes       = ["192.168.2.0/24"]
    service_endpoints = ["Microsoft.Storage","Microsoft.AzureActiveDirectory"]
}

resource "azurerm_subnet" "motsubnet" {
    name                 = "${var.prefix}-motsubnet"
    resource_group_name  = azurerm_resource_group.main.name
    virtual_network_name = azurerm_virtual_network.net.name
    address_prefixes       = ["192.168.3.0/24"]
    service_endpoints = ["Microsoft.Storage","Microsoft.Sql","Microsoft.AzureActiveDirectory"]
}

resource "azurerm_subnet" "epsubnet" {
    name                 = "${var.prefix}-epsubnet"
    resource_group_name  = azurerm_resource_group.main.name
    virtual_network_name = azurerm_virtual_network.net.name
    address_prefixes       = ["192.168.4.0/24"]
    service_endpoints = ["Microsoft.Storage","Microsoft.Sql","Microsoft.AzureActiveDirectory"]
}